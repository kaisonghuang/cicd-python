# CI/CD-python

This repo is used to play with Python Flask APP & Docker & CI/CD.

## 1. Install docker on CentOS 7
https://docs.docker.com/install/linux/docker-ce/centos/ <br/>
`NOTE`: Archived versions (e.g. CentOS 6) aren’t supported or tested. Also, use a 64-bit system as required by gitlab-runner.

I was stuck at this step:
```
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
```

So I replaced the above repo with http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo.

## 2. Use the CentOS server itself as a runner
For gitlab-runner installation, please refer to https://docs.gitlab.com/runner/install/linux-manually.html.<br/>
For registering the server, please refer to https://docs.gitlab.com/runner/register/.<br/>
`NOTE`: the __token__ can be found in __Settings__ -> __CI/CD__ -> __Runners__ -> __Expand__; the __tags__ are used in __.gitlab-ci.yml__ ([config parameters](https://docs.gitlab.com/ee/ci/yaml/)) to specify which runner you are using to run scripts.<br/>
`NOTE`:
```
# After you install and register GitLab runner (use root account)
sudo vim /etc/sudoers

# Add the line below and save the file
gitlab-runner ALL=(ALL) NOPASSWD: ALL

# Add gitlab-runner to docker group 
sudo usermod -aG docker gitlab-runner

# Restart gitlab-runner service
sudo gitlab-runner restart
```
#### Optional. Run gitlab-runner in the docker
Please refer to https://docs.gitlab.com/runner/install/docker.html.<br/>
`NOTE`: gitlab/gitlab-runner:latest is based on Ubuntu, in case you want to run in Alpine, you should do the following steps instead:
```
docker pull gitlab/gitlab-runner:alpine

docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:alpine
```

#### Optional. Register the docker as a runner
Please refer to https://docs.gitlab.com/runner/register/index.html#docker.<br/>

## 3. Write a REST API using Flask
Please refer to the code in my repo. For now, I have only implemented one __GET__ API at `localhost:5000/api/v1.0/task` if run locally using `python app.py`.

## 4. Convert Python Flask APP to a Docker container
#### 4.1. Create a Dockerfile
Create a `Dockerfile` (case sensitive) in the repo's main directory.
```
# from is used to fetch the base docker image
from alpine:latest

# RUN is executed when creating the image
# apk is the package manager for alpine linux
# --no-cache flag means do not use any local cache path
# python3-dev contains python3 and pip3
RUN apk add --no-cache python3-dev && pip3 install --upgrade pip
```
#### 4.2. Build the new image
Run the following command with your own image name. `.` will find the `Dockerfile` in the current directory.
```
docker build -t <new-image-name:tag> .
```
Once the building process is done, run `docker images` and you'll find the `flaskapp` image in your local docker image repository.

#### 4.3. Copy source code to the container
Open the `Dockerfile` again, and append the following snippet.
```
# WORKDIR command is similar to cd command
WORKDIR /app

# Copy everything from the directory the Dockerfile is in to /app inside the container
COPY . /app

# Install the required python packages
RUN pip3 --no-cache-dir install -r requirements.txt
```
Run the following command again with __the same__ &lt;image-name&gt;:&lt;tag&gt; you used above, the created image will be overwritten.
```
docker build -t <new-image-name:tag> .
```
`NOTE`: the building process will only execute changes.

#### 4.4. Make the container executable
Open the `Dockerfile` again, and append the following snippet.
```
# Port 5000 here is what we have specified in app.py, so we need to expose this port of the container when the app runs locally inside
EXPOSE 5000

# ENTRYPOINT command makes the container executable, which means the container will run the ENTRYPOINT command whenever it is created out of the image.
ENTRYPOINT ["python3"]

# CMD command is used to pass the arguments to the ENTRYPOINT, which makes python3 app.py
CMD ["app.py"]
```
Then run the same cmd `docker build ...` again.<br/>
Now, let's create a container out of the latest image with the following cmd.
```
docker run --name test -d -p 8080:5000 flaskapp
```
`--name` flag is used to assign a name to the container;<br/>
`-d` tells the container to run in daemon mode;<br/>
`-p <local-port>:<container-port>` binds the port of the container to the local port of the host machine.<br/>
You can test if the API is up and running on `localhost:8080/api/v1.0/task` by running `curl -X GET localhost:8080/api/v1.0/task`; and you should see a JSON response like this:
```
{
    "message": "inside get method"
}
```
## 5. Enable CI/CD
#### 5.1. Create a .gitlab-ci.yml
Add a `.gitlab-ci.yml` file in the main directory. Please refer to the yml file in this repo as a template.
```
stages:
  - test
  - deploy
  
test:
  stage: test
  only:
    - development
    - production
  tags:
    - runner199
  script:
    - docker-compose build

deploy2prod:
  stage: deploy
  only:
    - production
  tags:
    - runner199
  script:
    - docker image prune -f
    - docker-compose stop api_prod
    - docker-compose up -d api_prod
  environment: production
  when: manual
```
Here we have two stages __test__ and __deploy__.<br/>
`only`: the branches whose commits would trigger the stage to be executed.<br/>
`tags`: the runner where you want this stage to be executed, and the tags are what you have given to your specific runners.<br/>
`script`: the scripts to be executed in the runner's environment.<br/>
`environment`: where you want to deploy the product.<br/>
`when: manual`: this step has to be manually triggered.<br/>
#### 5.2. Create a .docker-compose.yml
To use docker-compose, you need to `pip install docker-compose` first. Create a `docker-compose.yml` file in the main directory. `docker-compose` can save us from wordy docker run commands, and it has a lot of practical configurations.
## 6. Check your container status
For now, the `.gitlab-ci.yml` will only deploy the container to the local machine, which means after the two stages, you will find your container up and running on your runner machine.
## 7. Host your own docker registry
#### 7.1. Prerequisites
1. A server that can be accessed remotely.
2. Install docker.
3. Install docker-compose.

#### 7.2. Create a docker-compose.yml in the registry server
```
---
version: '3'

services:
    docker-registry:
        container_name: docker-registry
        image: registry:2
        ports:
            - 5000:5000
        restart: always
        volumes:
            - ./volume:/var/lib/registry
    docker-registry-ui:
        container_name: docker-registry-ui
        image: konradkleine/docker-registry-frontend:v2
        ports:
            - 8080:80
        environment:
            ENV_DOCKER_REGISTRY_HOST: docker-registry
            ENV_DOCKER_REGISTRY_PORT: 5000
```
`- 5000:5000`: the left 5000 is your server's port which you want to expose to the outside world, and the right 5000 is what the container exposes from inside.<br/>
`- ./volume:/var/lib/registry`: the left dir is in your local file system where the right dir from the container is mounted to.<br/>
Then you need a UI to view the images on your registry. So you need to add another service `docker-registry-ui` which uses the image `konradkleine/docker-registry-frontend:v2`.<br/>
`ENV_DOCKER_REGISTRY_HOST`:YOUR-REGISTRY-HOST<br/>
`ENV_DOCKER_REGISTRY_PORT`:PORT-TO-YOUR-REGISTRY-HOST<br/>
Run `docker-compose -d up` to start the services. Go to `<registry host public ip>:<port you exposed>` (8080 in the above case), and you can see the frontend.<br/>

#### 7.3. Push images to the registry
For now, the docker registry is not using any authentication. So you need to configure the docker in the machine from which you want to push/pull images to the registry to enable insecure connections.<br/>
Add the following snippet to `/etc/docker/daemon.json`:
```
{
     "insecure-registries":["<your own registry hostname>:<port you exposed to access the docker registry backend>"]
}
```
Restart the docker service.<br/>
Then you need to create a release image based on your test image using __docker tag__. Here I use alpine image as an example, you can also replace the test image with any other local image.
```
docker tag <local image>:<tag> <registry hostname>:<backend port>/<username>/<image name>:<tag>
```
Here's my example:<br/>
`<local image>:<tag>`: alpine:latest<br/>
`<registry hostname>:<backend port>/<username>/<image name>:<tag>`: registry.example.com:5000/khuang/my_alpine:v1<br/>
Since you've created a new image, you can now push it to the registry using __docker push__.
```
docker push registry.example.com:5000/khuang/my_alpine:v1
```
If you want other docker clients to use this image, just configure the `daemon.json` file we mentioned above, and make sure your clients can reach your registry host, then use:
```
docker pull <registry hostname>:<backend port>/<username>/<image name>:<tag>
```
That's basically everthing you need to know. 

## TODO
1. Redesigning the pipeline.

## Troubleshooting
__Issue 1__:
```
sudo: sorry, you must have a tty to run sudo gitlab
```
__Solution__:
Please try `sudo vim /etc/sudoers`
and change the line `Defaults requiretty` to `Defaults:gitlab-runner !requiretty`.
